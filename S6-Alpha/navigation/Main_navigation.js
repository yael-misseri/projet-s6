import React from 'react';

//import react-navigation main modules
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//import every component's navigation file
import Signup_navigation from './Signup_navigation';
import Login_navigation from './Login_navigation';
import Profile_navigation from './Profile_navigation';

import {View, Text, Button} from 'react-native';
import {default_background_color, default_main_color } from '../styles/global_styles';

class Main_navigation extends React.Component{

  render(){
    const Stack = createStackNavigator();

    return (

        <NavigationContainer>

          <Stack.Navigator screenOptions={{headerBackTitleVisible: false}}>

          <Stack.Screen name ="Login_navigation" component={Login_navigation} options={{headerShown:false,

                                                                                      }} />
          <Stack.Screen name ="Signup_navigation" component={Signup_navigation} options={{ title : "Inscription",
                                                                                           headerStyle: {backgroundColor: default_main_color,},
                                                                                           headerTitleStyle:{color:"white"},
                                                                                           headerTintColor: 'white',
                                                                                         }}/>
          <Stack.Screen name ="Profile_navigation" component={Profile_navigation} options={{ title : "Profile",
                                                                                             headerStyle: {backgroundColor: default_main_color,},
                                                                                             headerTitleStyle:{color:"white"},
                                                                                             headerTintColor: 'white',
                                                                                          } } />


          </Stack.Navigator>
        </NavigationContainer>

    );

  }

}

export default Main_navigation;

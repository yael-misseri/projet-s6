import React from 'react';

//stack navigator
import { createStackNavigator } from '@react-navigation/stack';

//navigate into components:
import Login from '../components/login/Login';


function Login_navigation() {

  const Login_stack = createStackNavigator();

    return (
      <Login_stack.Navigator>

        <Login_stack.Screen name ="Login" component={Login} options={{headerShown:false}}/>

      </Login_stack.Navigator>

    );


}

export default Login_navigation;

import React from 'react';

//stack navigator
import { createStackNavigator } from '@react-navigation/stack';

//navigate into components:
import Profile from '../components/profile/Profile';


function Profile_navigation() {

  const Profile_stack = createStackNavigator();

    return (
      <Profile_stack.Navigator>

        <Profile_stack.Screen name ="Profile" component={Profile} options={{headerShown:false}} />

      </Profile_stack.Navigator>

    );


}

export default Profile_navigation;

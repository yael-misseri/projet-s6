import React from 'react';

//stack navigator
import { createStackNavigator } from '@react-navigation/stack';

//navigate into components:
import Signup from '../components/signup/Signup';
import Signup_name from '../components/signup/Signup_name';
import Signup_email from '../components/signup/Signup_email';
import Signup_birthdate from '../components/signup/Signup_birthdate';
import Signup_password from '../components/signup/Signup_password';


function Signup_navigation() {

  const Signup_stack = createStackNavigator();

    return (

      <Signup_stack.Navigator>

        <Signup_stack.Screen name ="Signup" component={Signup} options={{headerShown:false}}/>
        <Signup_stack.Screen name ="Signup_name" component={Signup_name} options={{headerShown:false}}/>
        <Signup_stack.Screen name ="Signup_email" component={Signup_email} options={{headerShown:false}} />
        <Signup_stack.Screen name ="Signup_birthdate" component={Signup_birthdate} options={{headerShown:false}} />
        <Signup_stack.Screen name ="Signup_password" component={Signup_password} options={{headerShown:false}} />


      </Signup_stack.Navigator>

    );
}

export default Signup_navigation;

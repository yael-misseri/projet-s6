//Import main: React
import React from 'react';

//Import database connection
//import connection from './database/db_connection';

//Enable redux architecture
import {Provider} from 'react-redux';
import Store from './store/configureStore';

//Import the main navigation system
import Main_navigation from './navigation/Main_navigation';



export default class App extends React.Component {

  render(){
    alert('Bienvenue sur la version de développement de notre projet ! Cette application est une base technique et applicative du concept. Toutes les fonctionnalités ne sont pas encore implémentées ! Pour obtenir tous les détails techniques du projet, merci de vous référer au rapport de projet. Bonne démonstration !');

    return (
      <Provider store={Store}>
        <Main_navigation/>
      </Provider>

    );
  }

}

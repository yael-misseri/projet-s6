const Connection = require('../../API/server/connection.js');
var moment = require('moment');

const initialState =

{


  email:null,
  usertype:null,
  mobilephone:null,
  name:null,
  surname:null,
  birthdate:null,
  address:null,
  city:null,
  status:null,
  work:null,
  school:null,
  isconnected:null,

  numberOfRelations:0,
  numberOfNetworks:0,

  isLoading:false,
  isError:false,
  isLoaded:false,

};

/**
login_reducer function is the reducer to manage all actions related to user login
*/
export default function profile_reducer(state=initialState,action){

  let nextState;

  switch (action.type){

    case "LOAD_PROFILE":

      let data = action.value;
      nextState = {...state,email:data.email,usertype:data.usertype,mobilephone:data.mobilephone,name:data.name,surname:data.surname,
                    birthdate:data.birthdate,address:data.address,city:data.city,isconnected:data.isconnected,isLoading:false,isLoaded:true};
      moment.locale('fr');
      let birth_format = moment(data.birthdate).format("DD/MM/YYYY");
      nextState.birthdate = birth_format;
      return nextState;
      break;

    case "ERROR_PROFILE":

      nextState = {...state,name:"Erreur",isLoading:false,isError:true};
      return nextState;
      break;

    case "LOADING_PROFILE":

      nextState = {...state,isLoading:true,isError:false,isLoaded:false};
      return nextState;
      break;

  }

  return state;

}


/*
let res;
let req = {

collection:"user",
action:"read",
record_id:action.value,

}

res= await Connection.get(req);

if(res.status==200){
  let data = res.data;
  nextState = {...state,email:data.email,usertype:data.usertype,mobilephone:data.mobilephone,name:data.name,surname:data.surname,birthdate:data.birthdate,address:data.address,city:data.city,isconnected:data.isconnected}
  if (nextState.name==null){ nextState.name="Utilisateur"}
  return nextState;
}
if (state.name==null){ console.log("TEST") ; state.name="Utilisateur"}
return state;*/

const Connection = require('../../API/server/connection.js');
const initialState =

{
  numberOfAttempts: 0,
};

/**
login_reducer function is the reducer to manage all actions related to user login
*/
export default function login_reducer(state=initialState,action){

  let nextState;

  switch (action.type){

    case 'CONNECT_USER':

      login(action.value[0], action.value[1],action.value[2],action.value[3],action.value[4]);
      nextState={...state, numberOfAttempts: state.numberOfAttempts+=1};
      return nextState;
      break;

    default: return state;

  }

}



/**
 * Try to login user, sending a POST request to server. Usage of Connecion class
 * @param {String} email - The entered email
 * @param {String} password - The enterd password
 * @return {Object} res - Request result
   @See <a href="Connection.html">Connection</a>
 */
const login = async function(email,password,callback,navigation,action_success){
  //req = array {collection => "collection_name (eg. user, etc.)",
  //             action=>"add OR update",
  //             record_id=> id of record for update [OPTIONAL],
  //             data=> { model attributes },
  //

  let loginInformations = {
    email:email,
    password:password,
   }

  let req = {collection:"user",action:"login",data:loginInformations};

  var res = await Connection.post(req);
  callback(res.status,res.data,navigation,action_success);
  return res;
}

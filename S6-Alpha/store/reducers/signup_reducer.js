//Reducer to manage actions from sign up form
const Connection = require('../../API/server/connection.js');
var moment = require('moment');

const initialState =
{
  userName:"",
  userSurname:"",
  userEmail:"",
  userBirthDate:new Date(),
  userPassword:"",

};

/**
signup_reducer function is the reducer to manage all actions related to user subscription
*/
export default function signup_reducer(state=initialState,action){

  let nextState;
  switch (action.type){

    case 'NAME_UPDATE':
      nextState={...state, userName:action.value};
      break;

    case 'SURNAME_UPDATE':
      nextState={...state, userSurname:action.value};
      break;

    case 'EMAIL_UPDATE':
      nextState={...state, userEmail:action.value};
      break;

    case 'BIRTHDATE_UPDATE':
      nextState={...state, userBirthDate:action.value};
      break;

    case 'PASSWORD_UPDATE':
      nextState={...state, userPassword:action.value};
      break;

    case 'SUBMIT_SUBSCRIPTION':
      submit(state);
      return state;
      break;

    default: return state;

  }

  return nextState;

}

/**
Submit all stored data of the reducer to the server. Used to create a new user in the DB.
Uses the post method of Connection class to send the new created user.
@param {Array} state - The state of the user reducer, containing all informations about the new userid
@return {void}
@See <a href="Connection.html">Connection</a>

*/
const submit = function(state){

  //req = array {collection => "collection_name (eg. user, etc.)",
  //             action=>"add OR update",
  //             record_id=> id of record for update [OPTIONAL],
  //             data=> { model attributes },
  //
  console.log(typeof state.userBirthDate);
  console.log(state.userBirthDate);

  let newUser = {

    email:state.userEmail,
    password:state.userPassword,
    usertype:"standard",
    name:state.userName,
    surname:state.userSurname,
    birthdate:state.userBirthDate,
    mobilephone:"12",

   }

  let req = {collection:"user",action:"add",data:newUser};

  Connection.post(req);

};

//Store file

import { createStore, combineReducers, applyMiddleware } from 'redux';


import signup_reducer from './reducers/signup_reducer';
import login_reducer from './reducers/login_reducer';
import profile_reducer from './reducers/profile_reducer';
import connection from '../API/server/connection'

var rootReducer = combineReducers({signup_reducer,login_reducer,profile_reducer,connection})
var store = createStore(rootReducer);
export default store;



let User = new Schema{

  userid: {type: Number, required: true},
  email: {type: String, required: true, maxlength:32},
  password: {type: String, required: true, maxlength:64},
  usertype: {type: String, required: true, maxlength:32},
  mobilephone: {type: Number, required:true},
  isconnected: {type: Boolean},

};

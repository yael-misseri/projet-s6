import axios from 'axios';

var configuration =

{
  server_address: "http://51.178.183.179",
  server_port: 4000,
  token: 0,
  current_user_id:0,
  lastRequestResult:null,

};

export default function connection(state=configuration,action){

  let nextState;

  switch (action.type){

    case "SET_USER_SECURITY_INFORMATIONS":
      nextState={...state,token:action.value.token,current_user_id:action.value.userid}
      configuration={...configuration,token:action.value.token,current_user_id:action.value.userid}
      return nextState;


    default: return state;

  }

}



  /**
  * post method is an asynchronous method used to send a POST request to server and get its response.
  * @version 1.0.0
  * @param {Object} req An array to represent the content of the request. This array must have this form:
  *                {collection: "model_name (eg. user, etc.)" ,
  *                action: "name of the action according to the server (eg. add,update etc.)",
  *                record_id: "id of record for update" [OPTIONAL],

  *                data: { model attributes }
  *
  * @return {Object} Returns an array of result. This array has the following form : {status:"HTTP response (eg. 200,404,403 etc.)", data:"text, value, object ... sent by server" }
  *
  */
  export async function post(req){

    configuration.lastRequestResult = null;

    console.log('Starting a new POST request ...');

    let collection = req['collection'];
    let action = req['action'];
    let record_id = req['record_id'];
    let data = req['data'];


    let param = "/"+configuration.token+"/"+configuration.current_user_id;
    if(action=="update"){
      param="/"+record_id;
    }


    let url = configuration.server_address + ":" + configuration.server_port + '/' + collection + '/' + action + param;

    console.log('POST request URL : ' + url);
    console.log('POST request : '); console.log(req);

    await axios.post(url,data).then(res=>configuration.lastRequestResult={"status":res.status,"data":res.data})
    .catch(err=>(typeof err.response ==="undefined") ? configuration.lastRequestResult={"status":null,"data":null} : configuration.lastRequestResult={"status":err.response.status,"data":err.response.data} );


    console.log("POST request result :"); console.log(configuration.lastRequestResult);
    return configuration.lastRequestResult;

  }

  /**
  * get method is an asynchronous method used to send a GET request to server and get its response.
  * @version 1.0.0
  * @param {Object} req An array to represent the content of the request. This array must have this form:
  *                {collection: "model_name (eg. user, etc.)" ,
  *                action: "name of the action according to the server (eg. read,delete etc.)",
  *                record_id: "id of record to work with",
  *                }
  *
  * @return {Object} Returns an array of result. This array has the following form : {status:"HTTP response (eg. 200,404,403 etc.)", data:"text, value, object ... sent by server" }
  *
  */
  export async function get(req){

    configuration.lastRequestResult = null;

    console.log('Starting a new GET request ...');

    let collection = req['collection'];
    let action = req['action'];
    let record_id = req['record_id'];

    let param="/"+configuration.token+"/"+configuration.current_user_id+"/"+record_id;

    let url = configuration.server_address + ":" + configuration.server_port + '/' + collection + '/' + action + param;

    console.log('GET request URL : ' + url);
    console.log('GET request : '); console.log(req);

    await axios.get(url).then(res=>configuration.lastRequestResult={"status":res.status,"data":res.data})
    .catch(err=>configuration.lastRequestResult={"status":err.response.status,"data":err.response.data});

    console.log("GET request result :"); console.log(configuration.lastRequestResult);
    return configuration.lastRequestResult;

  }

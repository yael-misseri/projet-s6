import axios from 'axios';
import store from '../../store/configureStore';

/**Class Connection - connect to the server <br>
USAGE: <br>
// 1. Import this file (const Connection = require('../../API/server/connection.js'); <br>
// 2. Directly call Connection methods eg: Connection.post({...})
*/
class Connection {

  /**
  Contains parameters to create a connection. Needs server_address and server_port. Stores the user token and the current_user_id to perform operations. The lastRequestResult is also stored.
  **/
  constructor(){

    this.server_address = "http://192.168.1.27";
  //this.server_address = "http://51.178.183.179"; //OVH
    this.server_port = 4000;

  }

  /**
  * post method is an asynchronous method used to send a POST request to server and get its response.
  * @version 1.0.0
  * @param {Object} req An array to represent the content of the request. This array must have this form:
  *                {collection: "model_name (eg. user, etc.)" ,
  *                action: "name of the action according to the server (eg. add,update etc.)",
  *                record_id: "id of record for update" [OPTIONAL],

  *                data: { model attributes }
  *
  * @return {Object} Returns an array of result. This array has the following form : {status:"HTTP response (eg. 200,404,403 etc.)", data:"text, value, object ... sent by server" }
  *
  */



  async post(req){


    this.lastRequestResult = null;

    console.log('Starting a new POST request ...');

    let collection = req['collection'];
    let action = req['action'];
    let record_id = req['record_id'];
    let data = req['data'];


    let param = "/"+this.token+"/"+this.current_user_id;
    if(action=="update"){
      param="/"+record_id;
    }


    let url = this.server_address + ":" + this.server_port + '/' + collection + '/' + action + param;

    console.log('POST request URL : ' + url);
    console.log('POST request : '); console.log(req);

    await axios.post(url,data).then(res=>this.lastRequestResult={"status":res.status,"data":res.data})
    .catch(err=>this.lastRequestResult={"status":err.response.status,"data":err.response.data});

    console.log("POST request result :"); console.log(this.lastRequestResult);
    return this.lastRequestResult;

  }

  /**
  * get method is an asynchronous method used to send a GET request to server and get its response.
  * @version 1.0.0
  * @param {Object} req An array to represent the content of the request. This array must have this form:
  *                {collection: "model_name (eg. user, etc.)" ,
  *                action: "name of the action according to the server (eg. read,delete etc.)",
  *                record_id: "id of record to work with",
  *                }
  *
  * @return {Object} Returns an array of result. This array has the following form : {status:"HTTP response (eg. 200,404,403 etc.)", data:"text, value, object ... sent by server" }
  *
  */
  async get(req){

    this.lastRequestResult = null;

    console.log('Starting a new GET request ...');

    let collection = req['collection'];
    let action = req['action'];
    let record_id = req['record_id'];

    let param="/"+this.token+"/"+this.current_user_id+"/"+record_id;

    let url = this.server_address + ":" + this.server_port + '/' + collection + '/' + action + param;

    console.log('GET request URL : ' + url);
    console.log('GET request : '); console.log(req);

    await axios.get(url).then(res=>this.lastRequestResult={"status":res.status,"data":res.data})
    .catch(err=>this.lastRequestResult={"status":err.response.status,"data":err.response.data});

    console.log("GET request result :"); console.log(this.lastRequestResult);
    return this.lastRequestResult;

  }


}

module.exports = new Connection();

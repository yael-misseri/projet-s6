export const

langage = {

  WELCOME_LABEL : "Join the network now !",
  WELCOME_TEXT :  "In a few seconds, create your account and begin to build your network ! \n (it's free)",

  START_BUTTON:"Start",
  CONDITIONS_TEXT:"General Conditions",

};

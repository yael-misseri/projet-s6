import React from 'react';
import { connect } from 'react-redux';
import {View, Button, Text, TextInput, TouchableOpacity} from 'react-native';
import {updatePassword_action, submitSubscription_action} from './signup_actions';
import { global_styles } from "../../styles/global_styles";
import { signup_styles } from "../../styles/signup/signup_styles";

class Signup_password extends React.Component {

  constructor(props){

    super(props);
    this.userPassword = null;
    this.confirmPassword=null;
  }

  handleActions(){

    if(!this._checkPasswordConfirmation()){
      alert('La confirmation ne correspond pas au mot de passe choisi.');
      return;
    }

    if(this._checkPasswordNotEmpty()){
      alert('Veuillez choisir un mot de passe pour continuer');
      return;
    }

  /*  if(!this._checkPasswordQuality()){
      alert('Le mot de passe choisis ne respecte pas les critères de sécurité.');
      return;
    }*/

    this.props.updatePassword_action(this.userPassword);
    this.props.submitSubscription_action();
    alert('Bienvenue ! Votre compte est maintenant créé. Vous pouvez vous y connecter.');
    this.props.navigation.navigate('Login');

  }

  _setUserPassword(value){
    this.userPassword=value;
  }

  _setConfirmPassword(value){
    this.confirmPassword=value;
  }

  _checkPasswordConfirmation(){
    return (this.userPassword==this.confirmPassword);
  }

  _checkPasswordNotEmpty(){
    return (this.userPassword=="" || this.userPassword==null);
  }

  _checkPasswordQuality(){

    var regex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*().]).{10,}/;
    var validPassword = regex.test(this.userPassword);
    return validPassword;

  }


  render(){
    return(

      <View style={global_styles.main_view}>

        <View style={signup_styles.title_view}>
          <Text style={global_styles.h2_title}>Choisissez un mot de passe fort</Text>
        </View>

        <View style={signup_styles.subtitle_view}>
          <Text style={global_styles.p_center_text}>Votre mot de passe doit contenir au moins 10 caractères dont un spécial, une majuscule ainsi qu'un chiffre. Notez-le bien !</Text>
        </View>

        <View style={signup_styles.center_view}>
          <TextInput secureTextEntry style={global_styles.standard_input} placeholder="Mot de passe" autoCompleteType="password" onChangeText={(value) => this._setUserPassword(value)}/>
          <TextInput secureTextEntry style={global_styles.standard_input} placeholder="Confirmer le mot de passe" autoCompleteType="password" onChangeText={(value) => this._setConfirmPassword(value)} />
          <TouchableOpacity style={global_styles.standard_button} onPress={()=>this.handleActions() }>
            <Text style={global_styles.standard_button_text}>Terminer</Text>
          </TouchableOpacity>

        </View>

        <View style={signup_styles.footer_view}>
          <Text style={global_styles.note_text}>Conditions générales</Text>
        </View>

      </View>


    );


  }

}

export default connect(state=>state,{updatePassword_action,submitSubscription_action})(Signup_password);

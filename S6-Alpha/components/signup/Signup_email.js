import React from 'react';
import { connect } from 'react-redux';
import {View, Button, Text, TextInput, TouchableOpacity} from 'react-native';
import {updateEmail_action} from './signup_actions';
import { global_styles } from "../../styles/global_styles";
import { signup_styles } from "../../styles/signup/signup_styles";

class Signup_email extends React.Component {

  constructor(props){

    super(props);
    this.userEmail = this.props.signup_reducer.userEmail;

  }

  handleActions(){

    if(!this._checkEmailValidity()){alert('Votre adresse email est importante pour continuer ! Nous ne nous enverrons pas de publicité, promis.'); return;}

    this.props.updateEmail_action(this.userEmail);
    this.props.navigation.navigate('Signup_birthdate');


  }

  _setUserEmail(value){
    this.userEmail=value;
  }

  _checkEmailValidity(){

    return !(this.userEmail=="" || this.userEmail==null);

  }


  render(){

    return(

      <View style={global_styles.main_view}>

        <View style={signup_styles.title_view}>
          <Text style={global_styles.h2_title}>Quelle est votre adresse email ?</Text>
        </View>

        <View style={signup_styles.center_view}>
          <TextInput keyboardType="email-address" style={global_styles.standard_input} defaultValue={this.userEmail} placeholder="Adresse email" onChangeText={(value) => this._setUserEmail(value)}/>
          <TouchableOpacity style={global_styles.standard_button} onPress={()=>this.handleActions() }>
            <Text style={global_styles.standard_button_text}>Suivant</Text>
          </TouchableOpacity>
        </View>

        <View style={signup_styles.footer_view}>
          <Text style={global_styles.note_text}>Conditions générales</Text>
        </View>

      </View>


    );


  }

}

export default connect(state=>state,{updateEmail_action})(Signup_email);

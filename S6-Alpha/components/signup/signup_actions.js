
export const updateName_action = newName => ({ type:"NAME_UPDATE", value:newName});

export const updateSurname_action = newSurname => ( {type:"SURNAME_UPDATE", value:newSurname});

export const updateEmail_action = newEmail => ( {type:"EMAIL_UPDATE", value:newEmail});

export const updateBirthDate_action = newDate => ( {type:"BIRTHDATE_UPDATE", value:newDate});

export const updatePassword_action = newPass => ( {type:"PASSWORD_UPDATE", value:newPass});

export const submitSubscription_action = () => ({type:"SUBMIT_SUBSCRIPTION"});

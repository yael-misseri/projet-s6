import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

//Import main config
import {config} from "../../config/config";

//Component config
const component_name = "signup"; //All in lowercase
const component_version = 1.0;

//Import sys_langage
import {langage} from "../../langage/fr/signup_langage";

//Import styles
import { global_styles } from "../../styles/global_styles";
import { signup_styles } from "../../styles/signup/signup_styles";

class Signup extends React.Component{

  _beginSubscription(){

    this.props.navigation.navigate('Signup_name');

  }  

  render(){

    return(

      <View style={global_styles.main_view}>

        <View style={signup_styles.title_view}>
          <Text style={global_styles.h2_title}>{langage.WELCOME_LABEL}</Text>
        </View>

        <View style={signup_styles.subtitle_view}>
          <Text style={global_styles.p_center_text}>{langage.WELCOME_TEXT}</Text>
        </View>

        <View style={signup_styles.center_view}>
          <TouchableOpacity style={global_styles.standard_button} onPress={()=>this._beginSubscription() }>
            <Text style={global_styles.standard_button_text}>{langage.START_BUTTON}</Text>
          </TouchableOpacity>
          <Text style={global_styles.note_text}>{langage.CONDITIONS_TEXT}</Text>

        </View>

        <View style={signup_styles.footer_image_view}>
            <Image style={signup_styles.banner_bottom} source={require('./resources/banner_bottom_crop.png')} />
        </View>

      </View>

    );


  };


}


export default Signup;

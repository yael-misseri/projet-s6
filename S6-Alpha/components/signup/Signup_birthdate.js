import React from 'react';
import { connect } from 'react-redux';
import {View, Button, Text, TextInput, TouchableOpacity} from 'react-native';
import {updateBirthDate_action} from './signup_actions';
import { global_styles } from "../../styles/global_styles";
import { signup_styles } from "../../styles/signup/signup_styles";
import DatePicker from 'react-native-datepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
var moment = require('moment');


class Signup_birthdate extends React.Component {

  constructor(props){

    super(props);
    this.state ={userBirthDate: this.props.signup_reducer.userBirthDate,
                 isDateTimePickerVisible: false,};

  }

  handleActions(){

    this.props.updateBirthDate_action(this.state.userBirthDate);
    this.props.navigation.navigate('Signup_password');

  }


  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  render(){

    var date_to_string = moment(this.state.userBirthDate).format("DD/MM/YYYY");
    return(

      <View style={global_styles.main_view}>

        <View style={signup_styles.title_view}>
          <Text style={global_styles.h2_title}>Quand êtes vous né ?</Text>
        </View>

        <View style={signup_styles.center_view}>

          <TouchableOpacity onPress={this._showDateTimePicker}>
            <TextInput style={global_styles.standard_input} placeholder="Date de naissance" editable={false} pointerEvents="none" value={date_to_string}/>
          </TouchableOpacity>

          <DateTimePickerModal
              isVisible={(this.state.isDateTimePickerVisible)}
              headerTextIOS="Choisissez votre date d'anniversaire"
              confirmTextIOS="Valider"
              cancelTextIOS="Annuler"
              mode="date"
              onConfirm={(value)=> this.setState({userBirthDate:value,isDateTimePickerVisible: false})}
              onCancel={this._hideDateTimePicker}
          />

          <TouchableOpacity style={global_styles.standard_button} onPress={()=>this.handleActions() }>
            <Text style={global_styles.standard_button_text}>Suivant</Text>
          </TouchableOpacity>
        </View>

        <View style={signup_styles.footer_view}>
          <Text style={global_styles.note_text}>Conditions générales</Text>
        </View>

      </View>


    );


  }

}

export default connect(state=>state,{updateBirthDate_action})(Signup_birthdate);

import React from 'react';
import { connect } from 'react-redux';
import {View, Button, Text, TextInput, TouchableOpacity} from 'react-native';
import {updateName_action, updateSurname_action} from './signup_actions';
import { global_styles } from "../../styles/global_styles";
import { signup_styles } from "../../styles/signup/signup_styles";

class Signup_name extends React.Component {

  /*
   USE:

      import * as actionCreators from './actionCreators'

      function mapStateToProps(state) {
        return { todos: state.todos }
      }

      export default connect(
        mapStateToProps,
        actionCreators
      )(TodoApp)

  */

  constructor(props){

    super(props);
    this.userName = this.props.signup_reducer.userName;
    this.userSurname = this.props.signup_reducer.userSurname;

  }

  handleActions(){

    if(!this._checkNamesNotEmpty()){alert('Nous aimerions faire connaissance. Veuillez saisir votre nom et votre prénom !'); return;}

    this.props.updateName_action(this.userName);
    this.props.updateSurname_action(this.userSurname);
    this.props.navigation.navigate('Signup_email');

  }

  _setUserName(value){
    this.userName=value;
  }

  _setUserSurname(value){
    this.userSurname=value;
  }

  _checkNamesNotEmpty(){

    return !(this.userName=="" || this.userName==null || this.userSurname=="" || this.userSurname==null)

  }

  render(){

    return(

      <View style={global_styles.main_view}>

        <View style={signup_styles.title_view}>
          <Text style={global_styles.h2_title}>Quel est votre nom?</Text>
        </View>

        <View style={signup_styles.center_view}>
          <TextInput style={global_styles.standard_input} defaultValue={this.userSurname} placeholder="Nom" onChangeText={(value) => this._setUserSurname(value)}/>
          <TextInput style={global_styles.standard_input} defaultValue={this.userName} placeholder="Prénom" onChangeText={(value) => this._setUserName(value)} />
          <TouchableOpacity style={global_styles.standard_button} onPress={()=>this.handleActions() }>
            <Text style={global_styles.standard_button_text}>Suivant</Text>
          </TouchableOpacity>

        </View>

        <View style={signup_styles.footer_view}>
          <Text style={global_styles.note_text}>Conditions générales</Text>
        </View>

      </View>


    );


  }

}

export default connect(state=>state,{updateName_action,updateSurname_action})(Signup_name);

import React from 'react';
import { connect } from 'react-redux';
import {View, Button, Text, TextInput, TouchableOpacity, Image, ImageBackground} from 'react-native';

//Import actions related to Profile
import {load_profile_action,error_profile_action,start_loading_profile_action} from './profile_actions';

import { global_styles } from "../../styles/global_styles";
import { profile_styles } from "../../styles/profile_styles";

const Connection = require('../../API/server/connection.js');


class Profile extends React.Component {

  constructor(props){
    super(props);
  }

  async _getUserData(){

    let res;

    let req = {

    collection:"user",
    action:"read",
    record_id:this.props.connection.current_user_id,

    }

    this.props.start_loading_profile_action();
    res = await Connection.get(req);

    if(res.status == 200){
      this.props.load_profile_action(res.data);
    }else{
      this.props.error_profile_action();
      //alert("Une erreur de connexion est servenue. Merci de réessayer.");
      //this.props.navigation.navigate("Login");
    }

  }

  componentDidMount(){
     this._getUserData();
  }


  render(){

    return(

      <View style={global_styles.main_view_grey}>

        <Image source={require('./resources/background.jpg')} style={profile_styles.wallpaper}>
        </Image>

            <View style={profile_styles.header_view}>

              <Image style={profile_styles.profile_picture} source={require('../../assets/avatar.png')} />

              <View style={profile_styles.header_name_view}>
                <Text style={global_styles.h3_title}>{this.props.profile_reducer.surname + " " + this.props.profile_reducer.name}</Text>
                <Text style={global_styles.note_text_slim_margin}>{this.props.profile_reducer.email}</Text>
                <Text style={global_styles.note_text_slim_margin}>0 points</Text>
              </View>

              <View style={profile_styles.header_settings_view}>
               <Image style={profile_styles.settings_picture} source={require('./resources/settings.png')} />
                <Image style={profile_styles.settings_picture} source={require('./resources/edit.png')} />
              </View>


             </View>





          <View style={profile_styles.nav_view}>

            <TouchableOpacity style={global_styles.list_button}>
              <Text style={global_styles.normal_text}>{"Relations (" + this.props.profile_reducer.numberOfRelations + ")"}</Text>
            </TouchableOpacity>

            <TouchableOpacity style={global_styles.list_button}>
              <Text style={global_styles.normal_text}>{"Réseaux (" + this.props.profile_reducer.numberOfNetworks + ")"}</Text>
            </TouchableOpacity>

          </View>

          <View style={profile_styles.about_view}>
              <Text style={global_styles.h3_title}>A propos</Text>

              <View style={profile_styles.about_view_infos}>

                <View style={global_styles.default_row_view}>
                  <Image style={profile_styles.about_icon} source={require('./resources/chat.png')} />
                  <Text style={global_styles.normal_text}>Salut ! Je développe mon réseau ici</Text>
                </View>

                <View style={global_styles.default_row_view}>
                  <Image style={profile_styles.about_icon} source={require('./resources/work.png')} />
                  <Text style={global_styles.normal_text}>Travail</Text>
                </View>

                <View style={global_styles.default_row_view}>
                  <Image style={profile_styles.about_icon} source={require('./resources/school.png')} />
                  <Text style={global_styles.normal_text}>Scolarité</Text>
                </View>

                <View style={global_styles.default_row_view}>
                  <Image style={profile_styles.about_icon} source={require('./resources/birthday.png')} />
                  <Text style={global_styles.normal_text}>Anniversaire</Text>
                </View>

              </View>

              <View style={global_styles.default_row_right_align_view}>
                <Image style={global_styles.edit_icon} source={require('./resources/edit.png')} />
              </View>

          </View>

          <View style={profile_styles.links_view}>
            <Text style={global_styles.h3_title}>Liens</Text>
            <View style={global_styles.default_row_right_align_view}></View>

            <View style={global_styles.default_row_left_align_view}>
              <Image style={profile_styles.link_icon} source={require('./resources/facebook.png')} />
              <Image style={profile_styles.link_icon} source={require('./resources/instagram.png')} />
              <Image style={profile_styles.link_icon} source={require('./resources/linkedin.png')} />
            </View>



            <View style={global_styles.default_row_right_align_view}>
              <Image style={global_styles.edit_icon} source={require('./resources/edit.png')} />
            </View>
          </View>


      </View>


    );


  }

}

export default connect(state=>state,{load_profile_action,error_profile_action,start_loading_profile_action})(Profile);

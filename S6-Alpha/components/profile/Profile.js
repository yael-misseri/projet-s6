import React from 'react';
import { connect } from 'react-redux';
import {View, ScrollView, Button, Text, TextInput, TouchableOpacity, Image, ImageBackground,ActivityIndicator} from 'react-native';

//Import actions related to Profile
import {load_profile_action,error_profile_action,start_loading_profile_action} from './profile_actions';

import { global_styles, default_main_color } from "../../styles/global_styles";
import { profile_styles } from "../../styles/profile_styles";

const Connection = require('../../API/server/connection.js');

class Profile extends React.Component {

  constructor(props){
    super(props);
  }

  async _getUserData(){

    let res;

    let req = {

    collection:"user",
    action:"read",
    record_id:this.props.connection.current_user_id,

    }

    this.props.start_loading_profile_action();
    res = await Connection.get(req);

    if(res.status == 200){
      this.props.load_profile_action(res.data);
    }else{
      this.props.error_profile_action();
      alert("Une erreur de connexion est survenue. Merci de réessayer.");
      this.props.navigation.navigate("Login");
    }

  }

  componentDidMount(){
     this._getUserData();
  }


  render(){

    var status = this.props.profile_reducer.status;
    if(status==null) {
      status = "Aucun statut renseigné" ;
    };

    var work = this.props.profile_reducer.work;
    if(work==null) {
      work = "Aucun poste renseigné" ;
    };

    var school = this.props.profile_reducer.school;
    if(school==null) {
      school = "Aucune formation renseignée" ;
    };

    if(this.props.profile_reducer.isLoading){
      return (
        <View style={global_styles.loading_view}>
          <ActivityIndicator size="large" color={default_main_color} />
        </View>
      );
    }

    return(

      <View style={global_styles.main_view_grey}>

        <Image source={require('./resources/background.jpg')} style={profile_styles.wallpaper}></Image>

        <View style={[profile_styles.bloc,profile_styles.header_bloc]}>

          <View style={profile_styles.bloc_header}>
            <Text style={profile_styles.bloc_title}></Text>
            <View style={profile_styles.edit_icon_view}>
              <TouchableOpacity>
                <Image style={profile_styles.edit_icon} source={require('./resources/edit.png')} />
              </TouchableOpacity>
            </View>

          </View>

          <Image style={profile_styles.profile_picture} source={require('../../assets/avatar.png')} />
          <Text style={profile_styles.user_name}>{this.props.profile_reducer.name  + " " + this.props.profile_reducer.surname}</Text>
          <Text style={profile_styles.user_email}>{this.props.profile_reducer.email}</Text>
        </View>

        <View style={[profile_styles.bloc,profile_styles.about_bloc]}>

          <View style={profile_styles.bloc_header}>
            <Text style={profile_styles.bloc_title}>À propos</Text>
            <View style={profile_styles.edit_icon_view}>
              <TouchableOpacity>
                <Image style={profile_styles.edit_icon} source={require('./resources/edit.png')} />
              </TouchableOpacity>
            </View>

          </View>

          <View style={[profile_styles.bloc_content, {alignItems:"center"}]}>

            <View style={global_styles.default_row_left_align_view}>
              <Image style={profile_styles.about_icon} source={require('./resources/chat.png')} />
              <Text style={global_styles.normal_text}>{status}</Text>
            </View>

            <View style={global_styles.default_row_left_align_view}>
              <Image style={profile_styles.about_icon} source={require('./resources/work.png')} />
              <Text style={global_styles.normal_text}>{work}</Text>
            </View>

            <View style={global_styles.default_row_left_align_view}>
              <Image style={profile_styles.about_icon} source={require('./resources/school.png')} />
              <Text style={global_styles.normal_text}>{school}</Text>
            </View>

            <View style={global_styles.default_row_left_align_view}>
              <Image style={profile_styles.about_icon} source={require('./resources/birthday.png')} />
              <Text style={global_styles.normal_text}>{this.props.profile_reducer.birthdate}</Text>
            </View>

          </View>

        </View>

        <View style={[profile_styles.bloc,profile_styles.social_networks_bloc]}>

          <View style={profile_styles.bloc_header}>
            <Text style={profile_styles.bloc_title}>Réseaux sociaux</Text>
            <View style={profile_styles.edit_icon_view}>
              <TouchableOpacity>
                <Image style={profile_styles.edit_icon} source={require('./resources/edit.png')} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={profile_styles.bloc_content}>
            <View style={global_styles.default_row_left_align_view}>
              <TouchableOpacity>
                <Image style={profile_styles.link_icon} source={require('./resources/facebook.png')} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image style={profile_styles.link_icon} source={require('./resources/instagram.png')} />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image style={profile_styles.link_icon} source={require('./resources/linkedin.png')} />
              </TouchableOpacity>
            </View>
          </View>

        </View>

        <View style={[profile_styles.bloc,profile_styles.relations_bloc]}>

          <View style={profile_styles.bloc_header}>
            <Text style={profile_styles.bloc_title}>Relations</Text>
            <View style={profile_styles.edit_icon_view}>
              <TouchableOpacity>
                <Image style={profile_styles.edit_icon} source={require('./resources/edit.png')} />
              </TouchableOpacity>
            </View>

          </View>

          <View style={profile_styles.bloc_content}>
            <TouchableOpacity style={global_styles.list_button}>
              <Text style={global_styles.normal_text}>{"Relations (" + this.props.profile_reducer.numberOfRelations + ")"}</Text>
            </TouchableOpacity>

            <TouchableOpacity style={global_styles.list_button}>
              <Text style={global_styles.normal_text}>{"Réseaux (" + this.props.profile_reducer.numberOfNetworks + ")"}</Text>
            </TouchableOpacity>
          </View>

        </View>

      </View>


    );


  }

}

export default connect(state=>state,{load_profile_action,error_profile_action,start_loading_profile_action})(Profile);

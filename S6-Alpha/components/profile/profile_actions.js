
export const load_profile_action = data => ({ type:"LOAD_PROFILE", value:data});

export const error_profile_action = () => ({type: "ERROR_PROFILE", value:null});

export const start_loading_profile_action = () =>({type:"LOADING_PROFILE", value:null});

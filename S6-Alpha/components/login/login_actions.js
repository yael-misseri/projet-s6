
/** Action to call the login method of the login_reducer
@param {Array} logins - email and password to try a connection
*/
export const connect_action = logins => ({ type:"CONNECT_USER", value:logins});

export const connect_success_action = data => ({ type:"SET_USER_SECURITY_INFORMATIONS", value:data});

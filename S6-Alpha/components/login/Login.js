import React from 'react';
import { connect } from 'react-redux';
import Connection from '../../API/server/connection.js';

import {StatusBar, View, Button, Text, TextInput, Image, TouchableOpacity, Platform} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';


//Import actions related to Login
import {connect_action,connect_success_action} from './login_actions';


//Import styles and component styles
import { global_styles, default_main_color } from "../../styles/global_styles";
import { login_styles } from "../../styles/login/login_styles";

/**
Login class - a React Native component
The Login class is the first screen of the application. It displays the UI used to perform login and access to a new account form.
*/


function StatusBarPlaceHolder() {
   const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

    if(Platform.OS==='ios'){
      //iOs:
      return (
          <View style={{
              width: "100%",
              height: STATUS_BAR_HEIGHT,
              backgroundColor: default_main_color
          }}>
              <StatusBar
                  barStyle="light-content"
              />
          </View>
      );

    }

    //Android:
    return(<StatusBar  barStyle="default" backgroundColor={default_main_color}/>);

}


class Login extends React.Component {

  constructor(props){

    super(props);
    this.loginInformations=["","",this._connectionThen,this.props.navigation.navigate,this.props.connect_success_action]; //0=> email; 1-> password; 2->callback ; 3->navigation screen after login

  }

  /**
  By clicking on 'Connect' button, this method is called
  It calls the connected action method (connected via Redux) "connect_action" of login_actions file
  @see <a href="login_actions.js.html">login_actions</a>
  */
  async handleActions(){

      if(this._checkNumberOfAttempts()){
        this.props.connect_action(this.loginInformations);
      }

  }

  /**
  Check the number on tentative for the Connection
  @return {Boolean} - True if it's ok, False if the limit is reached
  */
  _checkNumberOfAttempts(){

    if(this.props.login_reducer.numberOfAttempts>5){
      alert("Vous avez dépassé le nombre d'essai de connexion ... Veuillez redémarrer l'application ou cliquer sur \"Mot de passe oublié\" ");
      return false;
    }

    return true;

  }

  /**
  Callback function after a Connection
  @param {Integer} reqStatus - The status sent by the request - 200: connected - 404: user not found - 403: bad logins
  @param {Function} navigate - this.props.navigation.navigate function used to navigate to the main screen after a good Connection
  */
   async _connectionThen(reqStatus,reqData, navigate, action_success){

    if(reqStatus == 200 && reqData.userid && reqData.token){
      let userid = reqData.userid;
      let token = reqData.token;
      action_success({token:token,userid:userid});

      navigate('Profile_navigation');

    }else if(reqStatus == 404){
      alert("L'adresse email ou le numéro de téléphone ne correspond à aucun utilisateur.");
    }else if(reqStatus==403){
      alert("Le mot de passe est incorect.");
    }else{
      alert("Impossible de se connecter. Merci de vérifier votre connexion internet.")
    }
  }


  /**
  Set the class atribute email
  @param {String} email - The new email
  */
  _setEmail(email){
    this.loginInformations[0] = email;
  }

  /**
  Set the class atribute password
  @param {String} password - The new password
  */
  _setPassword(password){
    this.loginInformations[1] = password;
  }

  /**
  Navigates to the React-Navigation screen "Signup-navigation"
  */
  _goToSubscription(){
    this.props.navigation.navigate("Signup_navigation")
  }

  /**
     The React Native render method to generate and display the component
  */



//<StatusBar  barStyle="default" backgroundColor={default_main_color}/>
  render(){

    return(

      <SafeAreaView style={global_styles.main_view} forceInset={{top: 'never'}}>

              <StatusBarPlaceHolder/>

                <View style={login_styles.header_view}>
                  <TouchableOpacity>
                  <Image style={login_styles.logo} source={require('../../assets/logo_white.png')} />
                  </TouchableOpacity>
                </View>

                <View style={login_styles.center_view}>
                  <TextInput style={global_styles.standard_input} placeholder="Email ou mobile" onChangeText={(value) => this._setEmail(value)}/>
                  <TextInput secureTextEntry style={global_styles.standard_input} placeholder="Mot de Passe" onChangeText={(value) => this._setPassword(value)} />

                  <View style={login_styles.button_view}>
                    <TouchableOpacity style={global_styles.standard_button} onPress={()=> this.handleActions()}>
                      <Text style={global_styles.standard_button_text}>Connexion</Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity>
                  <Text style={global_styles.note_text}>Mot de passe oublié ?</Text>
                  </TouchableOpacity>
                </View>

                <View style={login_styles.footer_view} >
                  <TouchableOpacity style={global_styles.reverse_button} onPress={()=>this._goToSubscription() }>
                    <Text style={global_styles.reverse_button_text}>Créer un nouveau compte</Text>
                  </TouchableOpacity>
                </View>

        </SafeAreaView>

    );


  }

}

export default connect(state=>state,{connect_action,connect_success_action})(Login);

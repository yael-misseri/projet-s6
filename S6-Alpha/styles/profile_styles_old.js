import {StyleSheet, Platform} from 'react-native';
import { global_styles  } from './global_styles';
import { default_main_color, default_borderWidth, default_borderRadius } from './global_styles';



export const profile_styles = StyleSheet.create({


  //Views : --------------------------------------

  header_view:{

    flexDirection:"row",
    borderWidth:default_borderWidth,
    padding:10,
    flex: 0.15
    ,
    backgroundColor:"white",
    marginBottom:2,

  },


  header_name_view:{
    flexDirection:"column",
    borderWidth:default_borderWidth,
    alignItems:"flex-start",
    paddingLeft:5,
    flex:1.8,

  },

  header_settings_view:{
    flexDirection:"column",
    flex:0.2,
    borderWidth:0,
    alignItems:"flex-end",
    justifyContent:"flex-end",


  },

  nav_view:{
    borderWidth:default_borderWidth,
    padding:15,
    paddingTop:5,
    backgroundColor:"white",
    marginTop:2,
    marginBottom:2,
    flex:0.20,
    flexDirection:"column",
    justifyContent:"center",
  },

  about_view:{
    borderWidth:default_borderWidth,
    alignItems:"flex-start",
    padding:15,
    backgroundColor:"white",
    marginTop:2,
    marginBottom:2,
    flex:0.33,

  },

  about_view_infos:{

    marginTop:15,
    borderWidth:default_borderWidth,
    width:'100%',
  },


  links_view:{

    borderWidth:default_borderWidth,
    alignItems:"flex-start",
    padding:15,
    backgroundColor:"white",
    marginTop:2,
    marginBottom:2,
    flex:0.23,
  },

  //*********************IMAGES

  wallpaper:{

    height:"9%",
    width:"100%",
  },

  profile_picture:{

    width:130,
    height:130,
    borderWidth:default_borderWidth,
    borderRadius:80,
    bottom:50,
  },

  settings_picture:{

    marginBottom:15,
    flexWrap:'wrap',
    width:23,
    height:23,
    tintColor:default_main_color,
    borderWidth:default_borderWidth,

  },

  about_icon:{
    width:16,
    height:16,
    tintColor:default_main_color,
    marginRight:5,
    borderWidth:default_borderWidth,
  },

  link_icon:{
    width:45,
    height:45,
    marginTop:15,
    marginRight:15,
    borderWidth:default_borderWidth,
  },



});

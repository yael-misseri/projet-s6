import {StyleSheet} from 'react-native';
import { global_styles } from '../global_styles';
import { default_main_color, default_borderWidth, default_borderRadius, proportion } from '../global_styles';


export const login_styles = StyleSheet.create({

  logo:{

    width:proportion * 0.15,
    height:proportion * 0.15,
    borderWidth:default_borderWidth,

  },

  header_view:{

    backgroundColor : default_main_color,
    flex:1.25,
    justifyContent : "center",
    alignItems: "center",
    borderWidth:default_borderWidth,
  },

  center_view:{

    flex:2,
    marginRight:30,
    marginLeft:30,
    marginTop:50,
    borderWidth:default_borderWidth,


  },


  subtitle_view:{

    marginRight:30,
    marginLeft:30,
    borderWidth:default_borderWidth,

  },

  button_view:{

    marginTop:15,
    marginBottom:15,
    borderWidth:default_borderWidth,
  },

  footer_view:{

    flex:1,
    marginBottom:30,
    marginRight:30,
    marginLeft:30,
    justifyContent:"flex-end",
    borderWidth:default_borderWidth,


  },

});

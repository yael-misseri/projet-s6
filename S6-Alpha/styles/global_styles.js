import {StyleSheet,Dimensions} from 'react-native';

//greyblue: "#346775"
export const default_borderWidth = 0;
export const default_borderRadius= 3;
export const default_main_color = "#346775";
export const default_text_color = "#505050";
export const default_title_text_color = default_main_color;
export const default_background_color = "white";
export const proportion = Dimensions.get('window').height;
export const proportion_width = Dimensions.get('window').width;


export const global_styles = StyleSheet.create({

  //-----------------VIEWS

  loading_view:{
    flex: 1,
    justifyContent: "center",
    backgroundColor:default_background_color,
  },

  main_view:{

    flex:1,
    borderWidth:default_borderWidth,
    backgroundColor:default_background_color,

  },

  main_view_grey:{

    flex:1,
    borderWidth:default_borderWidth,
    backgroundColor:"lightgrey",
    //backgroundColor:default_main_color,

  },

  date_view:{

    flexDirection:"row",
    borderWidth:default_borderWidth,
  },


  default_row_view:{

    borderWidth:default_borderWidth,
    alignItems:"flex-start",
    flexDirection:"row",
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    width:'100%',


  },

  default_row_right_align_view:{

    borderWidth:default_borderWidth,
    flexDirection:"row",
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    justifyContent:"flex-end",
    width:'100%',

  },

  default_row_left_align_view:{

    borderWidth:default_borderWidth,
    flexDirection:"row",
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    justifyContent:"flex-start",
    width:'100%',

  },

  default_row_center_align_view:{

    borderWidth:default_borderWidth,
    flexDirection:"row",
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    justifyContent:"center",
    width:'100%',

  },




  //------------------TITLES, TEXTS and PARAGRAPHS

  h2_title:{

    color: default_title_text_color,
    borderWidth:default_borderWidth,
    textAlign:"center",
  //  fontWeight:"bold",
    fontSize:proportion * 0.025,
    //fontFamily:default_font,

  },

  h3_title:{

    color: default_title_text_color,
    borderWidth:default_borderWidth,
    textAlign:"center",
    //fontWeight:"bold",
    fontSize:proportion * 0.020,
    //fontFamily:default_font,

  },

  normal_text:{

    color:default_text_color,
    fontSize:proportion * 0.017,
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    borderWidth:default_borderWidth,
  },

  p_center_text:{

    color:default_text_color,
    textAlign:"center",
    fontSize:proportion * 0.017,
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    borderWidth:default_borderWidth,
  },

  p_text:{

    color:default_text_color,
    fontSize:proportion * 0.020,
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,
    borderWidth:default_borderWidth,
  },

  note_text:{

    color: default_main_color,
    borderWidth:default_borderWidth,
    textAlign:"center",
    fontSize:proportion * 0.013,
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,

  },

  note_text_slim:{

    color: default_text_color,
    fontSize:proportion * 0.010,
    borderWidth:default_borderWidth,
    textAlign:"center",
    marginTop:proportion * 0.002,
    marginBottom:proportion * 0.002,

  },



  //-----------------TEXT INPUTS

  standard_input:{

    borderWidth:default_borderWidth,
    borderWidth:0.3,
    borderColor:"grey",
    borderRadius:default_borderRadius,
    padding:6,
    marginBottom:proportion * 0.01,
    fontSize:proportion * 0.016,
    color:default_text_color,

  },

  standard_date_input:{

    borderWidth:default_borderWidth,
    flex:1,
  },


  //---------------BUTTONS

  standard_button:{

    borderWidth:default_borderWidth,
    marginTop:proportion * 0.01,
    marginBottom:proportion * 0.01,
    padding:8,
    borderRadius:default_borderRadius,
    backgroundColor:default_main_color,
    alignItems:"center",

  },

  standard_button_text:{

    borderWidth:default_borderWidth,
    fontSize:proportion * 0.016,
    color:"white",
  },

  reverse_button:{

    borderWidth:default_borderWidth,
    marginTop:proportion * 0.005,
    marginBottom:proportion * 0.005,
    padding:8,
    borderRadius:default_borderRadius,
    backgroundColor:default_background_color,
    borderWidth:default_borderWidth,
    borderColor:default_main_color,
    borderWidth:1,
    alignItems:"center",

  },

  reverse_button_text:{

    borderWidth:default_borderWidth,
    fontSize:proportion * 0.016,
    color:default_main_color,

  },

  list_button:{

    borderWidth:default_borderWidth,
    padding:8,
    backgroundColor:default_background_color,
    borderWidth:default_borderWidth,
    borderBottomWidth:0.3,
    textAlign:"left",

  },

  list_button_text:{

    borderWidth:default_borderWidth,
    fontSize:proportion * 0.016,
    color:default_text_color,
  },

  //-----------------------IMAGES ICONS


});

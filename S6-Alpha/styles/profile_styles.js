import {StyleSheet, Platform, Dimensions} from 'react-native';
import { global_styles  } from './global_styles';
import { default_main_color, default_borderWidth, default_borderRadius, default_background_color, default_text_color,default_title_text_color,proportion,proportion_width } from './global_styles';

var orientation="portrait";
if(proportion_width>proportion){
  orientation="landscape";
}

export const profile_styles = StyleSheet.create({


  //Views : --------------------------------------

    bloc:{

      marginTop:proportion * 0.002,
      marginBottom:proportion * 0.002,
      padding:proportion * 0.015,
      width:"100%",
      backgroundColor:default_background_color,
      borderWidth:default_borderWidth,
    },

    header_bloc:{

      marginTop:0,
      height:"13.8%",
      alignItems:"center",
    },

    about_bloc:{

      height:"27.7%",

    },

    social_networks_bloc:{

      height:"16.8%",
    },

    relations_bloc:{

      height:"26.3%",

    },

    bloc_header:{

      flexDirection:"row",
      width:"100%",

    },

    edit_icon_view:{

      borderWidth:default_borderWidth,
      flexDirection:"row",
      justifyContent:"flex-end",
      width:'50%',
    },

    bloc_content:{

      height:"100%",
      width:"100%",
      justifyContent:"center",

    },

  //Images : -------------------------------------

    wallpaper:{

      width:"100%",
      height:"13.8%",

    },

    profile_picture:{

      width:proportion* 0.15,
      height:proportion * 0.15,
      borderWidth:default_borderWidth,
      borderRadius:proportion* 0.085,
    //  bottom: (orientation==="portrait") ? (proportion*0.085) : (proportion_width * 0.085),
      bottom:proportion*0.085,
      position:"absolute",
      borderColor:default_background_color,
      borderWidth:3,

    },

    edit_icon:{

      width:proportion * 0.02,
      height:proportion * 0.02,
      tintColor:default_main_color,

    },

    about_icon:{

      width:proportion * 0.02,
      height:proportion * 0.02,
      marginRight:proportion * 0.01,
      tintColor:default_main_color,
      top:3,

    },

    link_icon:{

      width:proportion * 0.05,
      height:proportion * 0.05,
      marginRight:proportion * 0.01,

    },

  //Texts : -------------------------------------

  bloc_title:{

    width:"50%",
    color:default_title_text_color,
    fontSize:proportion * 0.02,
    //fontWeight:"bold",
  },

  user_name:{

    color:default_title_text_color,
    fontSize:proportion * 0.03,
    marginTop:proportion * 0.01,
//    fontWeight:"bold",

  },

  user_email:{

    color:default_text_color,
    fontSize:proportion * 0.015,

  },





});

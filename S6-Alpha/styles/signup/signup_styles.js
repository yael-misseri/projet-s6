import {StyleSheet} from 'react-native';
import { global_styles } from '../global_styles';
import { default_main_color, default_borderWidth, default_borderRadius } from '../global_styles';


export const signup_styles = StyleSheet.create({

  title_view:{

    flex:1,
    textAlign:"center",
    height:100,
    alignItems:"center",
    justifyContent:"center",
    borderWidth:default_borderWidth,
  },

  center_view:{

    flex:2,
    marginRight:30,
    marginLeft:30,
    paddingTop:30,
    borderWidth:default_borderWidth,



  },

  subtitle_view:{

    marginRight:30,
    marginLeft:30,
    borderWidth:default_borderWidth,

  },

  footer_view:{

    flex:1,
    marginBottom:30,
    justifyContent:"flex-end",
    borderWidth:default_borderWidth,


  },

  footer_image_view:{

    flex:1,
    justifyContent:"flex-end",
    alignItems:"center",
    borderWidth:default_borderWidth,

  },

  banner_bottom:{

    height:300,
    width:316.32,
    borderWidth:default_borderWidth,

  },

});
